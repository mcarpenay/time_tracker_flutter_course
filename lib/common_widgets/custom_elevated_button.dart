import 'package:flutter/material.dart';

class CustomElevatedButton extends StatelessWidget {
  CustomElevatedButton(
      {this.icon,
      this.color,
      this.textColor,
      this.buttonText,
      this.height: 50.0,
      this.onPressed});

  final Widget? icon;
  final Color? color; // button color
  final Color? textColor;
  final String? buttonText;
  final double? height;
  final VoidCallback? onPressed;

  //const CustomElevatedButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      child: ElevatedButton.icon(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          primary: color,
          onPrimary: Colors.indigo,
          padding: EdgeInsets.symmetric(horizontal: 32, vertical: 8),
        ),
        icon: Icon(
          Icons.security,
          size: 32,
          color: Colors.purple,
        ),
        label: Text(
          buttonText!,
          style: TextStyle(
            color: textColor,
            fontSize: 15.0,
          ),
        ),
      ),
    );
  }
}
