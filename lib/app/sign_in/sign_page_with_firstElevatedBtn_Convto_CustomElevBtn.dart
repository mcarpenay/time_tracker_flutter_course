//Video 103 and before

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:time_tracker_flutter_course/common_widgets/custom_elevated_button.dart';

class SignInPage extends StatelessWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Time Tracker'),
        elevation: 2.0,
      ),
      body: _buildContent(),
      backgroundColor: Colors.grey[200],
    );
  }

  Widget _buildContent() {
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(
            'Sign In',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 32,
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(height: 8.0),
          // ElevatedButton(
          //   style: ElevatedButton.styleFrom(
          //     primary: Colors.orange,
          //     onPrimary: Colors.white,
          //   ),
          //   onPressed: () {
          //     print('Button Pressed');
          //   },
          //   child: Text('Sign in with Google'),
          // ),
          ElevatedButton.icon(
            onPressed: () {},
            style: ElevatedButton.styleFrom(
              primary: Colors.white,
              onPrimary: Colors.indigo,
              padding: EdgeInsets.symmetric(horizontal: 32, vertical: 8),
            ),
            icon: Icon(
              Icons.security,
              size: 32,
              color: Colors.purple,
            ),
            label: Text(
              'Sign in with Google',
              style: TextStyle(
                color: Colors.black87,
                fontSize: 15.0,
              ),
            ),
          ),
          SizedBox(
            height: 8,
          ),
          CustomElevatedButton(
            icon: Icon(
              Icons.security,
              size: 32,
              color: Colors.purple,
            ),
            color: Colors.white,
            buttonText: 'Sign In With button',
            onPressed: () {},
          ),
        ],
      ),
    );
  }
}
