import 'package:time_tracker_flutter_course/common_widgets/custom_elevated_button.dart';
import 'package:flutter/material.dart';

class SignInButton extends CustomElevatedButton {
  SignInButton({
    Color? color,
    Color? textColor,
    String? buttonText,
    VoidCallback? onPressed,
  }) : super(
          icon: Icon(
            Icons.security,
            size: 32,
            color: Colors.purple,
          ),
          color: color,
          textColor: textColor,
          buttonText: buttonText,
          onPressed: onPressed,
        );
}
